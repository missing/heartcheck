package com.oshurkov.heartcheck.interactor

class MainInteractor {

    fun compareWords(one: String, two: String): Boolean {
        return one == two
    }

    fun additionTwo(a: Int): Int {
        return a + 3
    }

    fun constanta (): Int {
        return 7
    }
}