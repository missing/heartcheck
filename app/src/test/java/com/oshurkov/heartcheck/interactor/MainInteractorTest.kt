package com.oshurkov.heartcheck.interactor

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test


internal class MainInteractorTest{

    private val interactor = MainInteractor()

    @Test
    fun `compare two word correct`(){
        assertTrue(interactor.compareWords("one", "one"))
    }

    @Test
    fun `compare two word incorrect`(){
        assertFalse(interactor.compareWords("one", "two"))
    }
}